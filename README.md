Desktop version only

# Learning
- DOM manipulation
- Implement game logic
- Reset the game
- Set highscore

# How to use
Guess the number between 1 and 20:
- Write your number and click "CHECK!"
- On the right side, it will show whether your guess is too high or too low.
- Keep guessing until you are correct
